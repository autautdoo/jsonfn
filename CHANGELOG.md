# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.2](https://bitbucket.org/autautdoo/jsonfn/compare/v1.1.1...v1.1.2) (2019-04-16)



## [1.1.1](https://bitbucket.org/autautdoo/jsonfn/compare/v1.1.0...v1.1.1) (2019-04-16)



# 1.1.0 (2019-04-16)


### Features

* #IGPIMP-198 ([1fd1807](https://bitbucket.org/autautdoo/jsonfn/commits/1fd1807)), closes [#IGPIMP-198](https://bitbucket.org/autautdoo/jsonfn/issue/IGPIMP-198)
