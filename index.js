/**
 * Created by domagoj on 12/7/16.
 */

const JSONfn = {
  'parse': function(str) {
    return JSON.parse(str, function(k, v) {
      if (typeof v != 'string') {
        return v;
      } else {
        return JSONfn.parseFunction(v) || v;
      }
    });
  },
  'stringify': function(obj) {
    return JSON.stringify(obj, function(k, v) {
      return (typeof v == 'function') ? v.toString() : v;
    });
  },
  'parseFunction': function(str) {
    const funcReg = /function\s*\w*\s*\(([^()]*)\)[ \n\t]*\{(.*)}/gmi;
    const match = funcReg.exec(str.replace(/\n/g, ' '));
    if (!match) {
      return null;
    }
    const args = match[1].split(',').map((a) => a.trim());
    const fnBody = match[2];
    return new Function(args, fnBody);
  },
};

module.exports = JSONfn;
